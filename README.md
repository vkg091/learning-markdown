![Hudelabs Logo](http://tribeout.com/hudelabs-logo.png)
# Quiz API  
Quiz API Enables users to fetch questions categorised into categories and topics.
## Gettings Started  
These instructions will get you up and running on your local machine development and testing purpose. See deployment for notes on how to deploy the project on a live system.  
### Prerequisites  
What things you need to install the software and how to install them. You will need [Node,js >= 7.0.0](https://nodejs.org/en/download/) and [NPM >= 4.0.0](https://www.npmjs.com/)  
Run the following commands to setup a local copy of the software.
```  
    git clone https://github.com/pareshchouhan/test-git.git
```  
### Installing  
To get your server up and running run the following commands, make sure you have cloned the [repository](git clone https://github.com/pareshchouhan/test-git.git) before proceeding.
```shell
cd test-git
npm install
npm start
```
Wait until you see the following output
```
INFO    - Building documentation...
INFO    - Cleaning site directory
[I 170309 16:07:05 server:283] Serving on http://127.0.0.1:8000  
[I 170309 16:07:05 handlers:60] Start awtching changes
[I 170309 16:07:05 handlers:62] Start detecting changes
```

Now open a web browser and run `http:hudelabs.locel/phpmyadmin` to see if everything is up and running.

## Running the tests
```shell
npm install
npm run test
```

```javascript
var i =0;
for( i = 30; i > 0 ; i-- ){
    console.log('It Works');
}
```
### Deployment
To deploy the system on production servers.
- Make sure you have [Node.js](http://nodejs.org)
- Required Kernel version 4.4.0
- Requires php-fpm installed
- Required php-7.1

### Built With
- [Dropwizard](The web framework) The web framework used
- [Maven](http://maven.org) Dependency Management
- [ROME](http://rome.org) Used to generate RSS Feeds

### Versioning
We use [SemVer](http://semver.com) for versioning. For the versions available, see the [tags on this repository.](http://tags.com)

### Authors
- **Vikas Gupta** - initial work - [VikasGutpta](http://vikasgupta.me)
See also the list of [contributors](http://contributors.com) who participated in this project

### License
This project is licensed under the MTI License - see the [LICENSE.md](http://license.com) frile for details


### Acknowledgments
- Hat tip to anyone who's code was used
- Inspiration
- etc


